<?php
/*
	Plugin Name:  Add Image
	Plugin URL: http://www.example.org/
	Version: 1.0
	Author: Rafael Figueredo
	Author URL: http://www.example.org/
	Description: This plugin automatically adds an image at the beginning of every post
	
*/

	function add_image($post)
	{
		$imagewithtext = "<img src='http://lorempixel.com/400/200' height='70px' width='70px' align='left' />";
		$imagewithtext .= $post;
		return $imagewithtext;
	}

	add_filter( 'the_content', 'add_image');